<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Adam Wilson',
            'email' => 'info@materialtech.co',
            'password' => bcrypt('Material4tech'),
        ]);

        DB::table('users')->insert([
            'name' => 'Sam',
            'email' => 'sam@yeomanlandscaping.co.uk',
            'password' => bcrypt('password'),
        ]);

    }
}
