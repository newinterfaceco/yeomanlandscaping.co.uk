<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'FrontendController@index')->name('index');

Route::get('maintenance', 'FrontendController@maintenance')->name('maintenance');

Route::get('hard-landscaping', 'FrontendController@hard_landscaping')->name('hard-landscaping');

Route::get('tree-surgery', 'FrontendController@tree_surgery')->name('tree-surgery');

Route::get('corporate-landscaping', 'FrontendController@corporate_landscaping')->name('corporate-landscaping');

Route::get('garden-design', 'FrontendController@garden_design')->name('garden-design');

Route::get('pimp-your-garden', 'FrontendController@pimp_your_garden')->name('pimp-your-garden');

Route::get('gallery', 'FrontendController@gallery')->name('gallery');

Route::get('contact', 'FrontendController@contact')->name('contact');

Route::post('contact', 'ContactController@request');

Auth::routes();

Route::get('logout', 'LogoutController@logout');

Route::resource('admin/dashboard', 'DashboardController');

Route::resource('admin/gallery', 'GalleryController');

Route::resource('admin/testimonial', 'TestimonialController');

Route::resource('admin/password', 'UserController');

Route::get('/img/{width}/{length}/{file}.{ext}', function($width, $length, $file, $ext)
{
    $img = \Image::make(public_path() . '/images/' . $file . '.' . $ext)->resize($width, $length);

    return $img->response($ext);
});

// 301 routes

Route::get('admin', function () {
    return redirect('/login', 301);
});

Route::get('home', function () {
    return redirect('/admin/dashboard', 301);
});

// 404 routes

Route::get('register', function () {
   return Response::view('errors/404', array(), 404);
});

Route::get('password/email', function () {
   return Response::view('errors/404', array(), 404);
});

Route::get('password/reset', function () {
    return Response::view('errors/404', array(), 404);
});
