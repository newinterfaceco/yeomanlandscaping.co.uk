<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Mail\ContactRequest;

use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller
{

	public function request(ContactFormRequest $request)
    {

    	\Mail::to('sam@yeomanlandscaping.co.uk')->send(new ContactRequest($request));

	    \Mail::to('adam@newinterface.co')->send(new ContactRequest($request));

    	return back()->with('contact_message', 'Thanks for contacting us!');

    }

}