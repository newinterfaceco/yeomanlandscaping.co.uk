<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Testimonial;

use App\Image;

class FrontendController extends Controller
{

    public function index()
    {

        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('index', ['testimonial' => $testimonials->random(1)]);
        } else {
            return view('index');
        }

    }

    public function maintenance()
    {

        $images = Image::where('service', '=', '1')->get();

        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('maintenance', ['testimonial' => $testimonials->random(1), 'images' => $images]);
        } else {
            return view('maintenance');
        }
    }

    public function hard_landscaping()
    {

        $images = Image::where('service', '=', '2')->get();

        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('hard-landscaping', ['testimonial' => $testimonials->random(1), 'images' => $images]);
        } else {
            return view('hard-landscaping');
        }
    }

    public function tree_surgery()
    {

        $images = Image::where('service', '=', '3')->get(); 

        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('tree-surgery', ['testimonial' => $testimonials->random(1), 'images' => $images]);
        } else {
            return view('tree-surgery');
        }
    }

    public function corporate_landscaping()
    {

        $images = Image::where('service', '=', '4')->get();

        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('corporate-landscaping', ['testimonial' => $testimonials->random(1), 'images' => $images]);
        } else {
            return view('corporate-landscaping');
        }
    }

    public function garden_design()
    {

        $images = Image::where('service', '=', '5')->get(); 

        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('garden-design', ['testimonial' => $testimonials->random(1), 'images' => $images]);
        } else {
            return view('garden-design');
        }
    }

    public function pimp_your_garden()
    {

        $images = Image::where('service', '=', '6')->get();

        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('pimp-your-garden', ['testimonial' => $testimonials->random(1), 'images' => $images]);
        } else {
            return view('pimp-your-garden');
        }
    }

    public function contact()
    {
        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('contact', ['testimonial' => $testimonials->random(1)]);
        } else {
            return view('contact');
        }
    }

    public function gallery()
    {

        $images = Image::all();
        $testimonials = Testimonial::all();

        if($testimonials->count()) {
            return view('gallery', ['testimonial' => $testimonials->random(1), 'images' => $images]);
        } else {
            return view('gallery', ['images' => $images]);
        }
    }

}
