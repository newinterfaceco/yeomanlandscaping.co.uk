<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function logout()
    {
        \Auth::logout();
        session()->flash('logout', 'You have been logged out!');
        return redirect('/');
    }
    
}
