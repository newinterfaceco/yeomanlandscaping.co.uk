(function($){
  $(function(){

  	$('.button-collapse').sideNav();
  	
    $('.parallax').parallax();

    $('select').material_select();

  	$('.slider').slider({full_width: true, height: 500, indicators: true, interval: 20000});

  	$('.materialboxed').materialbox();
    
  }); // end of document ready
})(jQuery); // end of jQuery name space