if(window.location.pathname == "/admin/gallery")
{

    setInterval(function(){ 
        
        $.ajax({
            dataType: "json",
            cache: false,
            url: '/admin/gallery/1',
            success: function (result) {

                if(result.images.length != 0) {

                	if ($('#maintenance-gallery .empty').length != 0) {
                		$('#maintenance-gallery').empty();
                	}

                    result.images.forEach(function(image) {

                    	if($('#maintenance-gallery').find("#" + image.id).length == 0) {
                    		$('#maintenance-gallery').append('<div class="admin-gallery-image"><img id="' + image.id + '" src="/img/250/250/' + image.url + '"><a href="/admin/gallery/' + image.id + '/edit" class="delete-gallery-image"><i class="material-icons white red-text">close</i></a></div>');
                    	} 

                    });

                } else {
                	if($('#maintenance-gallery').html().length == 0) {
                		$('#maintenance-gallery').append('<p class="empty">There are no images for this service.</p>');
                	}
                }

            },
        });
        
    }, 500);

    setInterval(function(){ 
        
        $.ajax({
            dataType: "json",
            cache: false,
            url: '/admin/gallery/2',
            success: function (result) {

                if(result.images.length != 0) {

                	if ($('#landscaping-gallery .empty').length != 0) {
                		$('#landscaping-gallery').empty();
                	}

                    result.images.forEach(function(image) {

                    	if($('#landscaping-gallery').find("#" + image.id).length == 0) {
                    		$('#landscaping-gallery').append('<div class="admin-gallery-image"><img id="' + image.id + '" src="/img/250/250/' + image.url + '"><a href="/admin/gallery/' + image.id + '/edit" class="delete-gallery-image"><i class="material-icons white red-text">close</i></a></div>');
                    	} 

                    });

                } else {
                	if($('#landscaping-gallery').html().length == 0) {
                		$('#landscaping-gallery').append('<p class="empty">There are no images for this service.</p>');
                	}
                }

            },
        });
        
    }, 500);

    setInterval(function(){ 
        
        $.ajax({
            dataType: "json",
            cache: false,
            url: '/admin/gallery/3',
            success: function (result) {

                if(result.images.length != 0) {

                	if ($('#tree-gallery .empty').length != 0) {
                		$('#tree-gallery').empty();
                	}

                    result.images.forEach(function(image) {

                    	if($('#tree-gallery').find("#" + image.id).length == 0) {
                    		$('#tree-gallery').append('<div class="admin-gallery-image"><img id="' + image.id + '" src="/img/250/250/' + image.url + '"><a href="/admin/gallery/' + image.id + '/edit" class="delete-gallery-image"><i class="material-icons white red-text">close</i></a></div>');
                    	} 

                    });

                } else {
                	if($('#tree-gallery').html().length == 0) {
                		$('#tree-gallery').append('<p class="empty">There are no images for this service.</p>');
                	}
                }

            },
        });
        
    }, 500);

    setInterval(function(){ 
        
        $.ajax({
            dataType: "json",
            cache: false,
            url: '/admin/gallery/4',
            success: function (result) {

                if(result.images.length != 0) {

                	if ($('#corporate-gallery .empty').length != 0) {
                		$('#corporate-gallery').empty();
                	}

                    result.images.forEach(function(image) {

                    	if($('#corporate-gallery').find("#" + image.id).length == 0) {
                    		$('#corporate-gallery').append('<div class="admin-gallery-image"><img id="' + image.id + '" src="/img/250/250/' + image.url + '"><a href="/admin/gallery/' + image.id + '/edit" class="delete-gallery-image"><i class="material-icons white red-text">close</i></a></div>');
                    	} 

                    });

                } else {
                	if($('#corporate-gallery').html().length == 0) {
                		$('#corporate-gallery').append('<p class="empty">There are no images for this service.</p>');
                	}
                }

            },
        });
        
    }, 500);

    setInterval(function(){ 
        
        $.ajax({
            dataType: "json",
            cache: false,
            url: '/admin/gallery/5',
            success: function (result) {

                if(result.images.length != 0) {

                	if ($('#design-gallery .empty').length != 0) {
                		$('#design-gallery').empty();
                	}

                    result.images.forEach(function(image) {

                    	if($('#design-gallery').find("#" + image.id).length == 0) {
                    		$('#design-gallery').append('<div class="admin-gallery-image"><img id="' + image.id + '" src="/img/250/250/' + image.url + '"><a href="/admin/gallery/' + image.id + '/edit" class="delete-gallery-image"><i class="material-icons white red-text">close</i></a></div>');
                    	} 

                    });

                } else {
                	if($('#design-gallery').html().length == 0) {
                		$('#design-gallery').append('<p class="empty">There are no images for this service.</p>');
                	}
                }

            },
        });
        
    }, 500);

    setInterval(function(){ 
        
        $.ajax({
            dataType: "json",
            cache: false,
            url: '/admin/gallery/6',
            success: function (result) {

                if(result.images.length != 0) {

                	if ($('#pimp-gallery .empty').length != 0) {
                		$('#pimp-gallery').empty();
                	}

                    result.images.forEach(function(image) {

                    	if($('#pimp-gallery').find("#" + image.id).length == 0) {
                    		$('#pimp-gallery').append('<div class="admin-gallery-image"><img id="' + image.id + '" src="/img/250/250/' + image.url + '"><a href="/admin/gallery/' + image.id + '/edit" class="delete-gallery-image"><i class="material-icons white red-text">close</i></a></div>');
                    	} 

                    });

                } else {
                	if($('#pimp-gallery').html().length == 0) {
                		$('#pimp-gallery').append('<p class="empty">There are no images for this service.</p>');
                	}
                }

            },
        });
        
    }, 500);

}