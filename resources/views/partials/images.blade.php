@if(isset($images))
	@if($images->count() == 0)
		<div class="row">
			<div class="col s12 m6 l4">
				<p>There are no images in this portfolio.</p>
			</div>
		</div>
	@else 
		<div class="row">
			@foreach($images as $image)
				<div class="col s12 m6 l4">
					<div class="material-placeholder"><img class="materialboxed gallery-image" src="/img/750/750/{{ $image->url }}"></div>
				</div>
			@endforeach
		</div>
	@endif
@endif