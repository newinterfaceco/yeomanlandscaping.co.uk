<footer>
    
    <div class="container">
        <div class="row">

            <div class="col l4 s12">
                <h5 class="white-text">{{ config('app.name') }}</h5>
                <ul>
                    <li><a class="white-text light""><span class="normal">Address:</span>&nbsp; {{ config('app.address') }}</a></li>
                    <li><a class="white-text light""><span class="normal">Postcode:</span>&nbsp; {{ config('app.postcode') }}</a></li>
                    <li><a class="white-text light" href="mailto:{{ config('app.email') }}"><span class="normal">Email:</span>&nbsp; {{ config('app.email') }}</a></li>
                    <li><a class="white-text light" href="tel:{{ config('app.landline') }}"><span class="normal">Landline:</span>&nbsp; {{ config('app.landline') }}</a></li>
                    <li><a class="white-text light" href="{{ config('app.mobile') }}"><span class="normal">Mobile:</span>&nbsp; {{ config('app.mobile') }}</a></li>
                </ul>
            </div>

             <div class="col l3 s12">
                <h5 class="white-text">Pages</h5>
                <ul>
                    <li><a class="white-text light" href="/">Yeoman Landscaping</a></li>
                    <li><a class="white-text light" href="/maintenance">Maintenance</a></li>
                    <li><a class="white-text light" href="/hard-landscaping">Hard Landscaping</a></li>
                    <li><a class="white-text light" href="/tree-surgery">Tree Surgery</a></li>
                    <li><a class="white-text light" href="/corporate-landscaping">Corporate Landscaping</a></li>
                    <li><a class="white-text light" href="/garden-design">Garden Design</a></li>
                    <li><a class="white-text light" href="/pimp-your-garden">Pimp Your Garden</a></li>
                    <li><a class="white-text light" href="/gallery">Gallery</a></li>
                    <li><a class="white-text light" href="/contact">Contact</a></li>
                </ul>
            </div>

             <div class="col l3 s12">
                <h5 class="white-text">Useful Links</h5>
                <ul>
                    <li><a class="white-text light" href="/contact">Contact</a></li>
                </ul>
            </div>

            <div class="col l2 s12">
                <h5 class="white-text">Social</h5>
                <ul>
                    <li><a class="white-text light" href="{{ config('app.facebook') }}">Facebook</a></li>
                    <li><a class="white-text light" href="{{ config('app.twitter') }}">Twitter</a></li>
                    <li><a class="white-text light" href="{{ config('app.instagram') }}">Instagram</a></li>
                    <li><a class="white-text light" href="{{ config('app.google') }}">Google +</a></li>
                </ul>
            </div>

        </div>
    </div>

    <div class="footer-copyright">
        <div class="container dark-grey-text darken-4">
            &copy; 2016 Yeoman Landscaping. All Rights Reserved <!-- | <a href="#">Material Web Design</a> -->
        </div>
    </div>
</footer>