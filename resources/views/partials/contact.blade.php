@if(isset($testimonial))
	<div class="parallax-container valign-wrapper full-width">
		<div class="container">
			<div class="row center">
	  			<h5 class="header col s12 light white-text testimonial-text"><i class="material-icons">format_quote</i>{{ $testimonial->content }}
	            <i class="material-icons">format_quote</i><br><br><span class="testimonial-title">{{ $testimonial->name }}</span></h5>
			</div>
			</div>
		<div class="parallax"><img src="/slides/3.JPG" alt="Unsplashed background img 2"></div>
	</div>
@endif
<section>
	<div class="container">
		<div class="row">

            <div class="col s12">
                <div class="card contact-card">
                    <div class="row">
						<div class="col s12 m6">
							<img src="/sam.png" alt="Sam Working" width="100%" style="margin-bottom: -6px;">
						</div>
						<div class="col s12 m6">
							<div class="contact-form">
							    <form class="col s12" action="/contact" method="POST">
							    	{{ csrf_field() }}

						      		<div class="row">

								        <div class="input-field col s6">
								          	<input name="name" id="name" type="text" class="validate">
								          	<label for="name">Name</label>
								        </div>

								        <div class="input-field col s6">
								          	<input name="subject" id="subject" type="text" class="validate">
								          	<label for="subject">Subject</label>
								        </div>

								        <div class="input-field col s12">
								          	<input name="email" id="email" type="text" class="validate">
								          	<label for="email">Email</label>
								        </div>

								        <div class="input-field col s12">
							          		<input name="user_message" id="message" type="text" class="validate textarea">
								          	<label for="message">Message</label>
								        </div>

						      		</div>

						      		<button type="submit" class="btn-large waves-effect green darken-4 white-text right">Submit</button>

							    </form>
						    </div>

						</div>
					</div>
                </div>
            </div>

		</div>
	</section>