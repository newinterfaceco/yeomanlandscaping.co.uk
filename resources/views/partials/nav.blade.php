<nav class="primary" role="navigation">
	<div class="nav-wrapper container">

		<a href="/" class="brand-logo"><img src="/logo.png" alt="Logo"></a>

		<ul class="right hide-on-med-and-down">
			<li><a href="mailto:{{ config('app.email') }}"><i class="material-icons left">mail_outline</i>{{ config('app.email') }}</a></li>
			<li><a href="tel:{{ config('app.landline') }}"><i class="material-icons left">phone</i>{{ config('app.landline') }}</a></li>
			<li><a href="tel:{{ config('app.mobile') }}"><i class="material-icons left">phone_iphone</i>{{ config('app.mobile') }}</a></li>
		</ul>
	
	</div>
</nav>



<nav class="secondary" role="navigation">
	<div class="nav-wrapper container">

		<ul class="main-navigation">
			<li class="{{ Request::path() == '/' ? 'active' : '' }}"><a href="/" class="dark-green-text poppins">Yeoman Landscaping</a></li>
	        <li class="{{ Request::path() == 'maintenance' ? 'active' : '' }}"><a href="/maintenance" class="dark-green-text poppins">Maintenance</a></li>
	        <li class="{{ Request::path() == 'hard-landscaping' ? 'active' : '' }}"><a href="/hard-landscaping" class="dark-green-text poppins">Hard Landscaping</a></li>
	        <li class="{{ Request::path() == 'tree-surgery' ? 'active' : '' }}"><a href="/tree-surgery" class="dark-green-text poppins">Tree Surgery</a></li>
	        <li class="{{ Request::path() == 'corporate-landscaping' ? 'active' : '' }}"><a href="/corporate-landscaping" class="dark-green-text poppins">Corporate Landscaping</a></li>
	        <li class="{{ Request::path() == 'garden-design' ? 'active' : '' }}"><a href="/garden-design" class="dark-green-text poppins">Garden Design</a></li>
	        <li class="{{ Request::path() == 'pimp-your-garden' ? 'active' : '' }}"><a href="/pimp-your-garden" class="dark-green-text poppins">Pimp Your Garden!</a></li>
	        <li class="{{ Request::path() == 'gallery' ? 'active' : '' }}"><a href="{{ route('gallery') }}" class="dark-green-text poppins">Gallery</a></li>
	        <li class="{{ Request::path() == 'contact' ? 'active' : '' }}"><a href="{{ route('contact') }}" class="dark-green-text poppins">Contact Us</a></li>
		</ul>

		<ul id="nav-mobile" class="main-navigation-mobile side-nav">
			<li class="{{ Request::path() == '/' ? 'active' : '' }}"><a href="/" class="dark-green-text poppins">Yeoman Landscaping</a></li>
	        <li class="{{ Request::path() == 'maintenance' ? 'active' : '' }}"><a href="/maintenance" class="dark-green-text poppins">Maintenance</a></li>
	        <li class="{{ Request::path() == 'hard-landscaping' ? 'active' : '' }}"><a href="/hard-landscaping" class="dark-green-text poppins">Hard Landscaping</a></li>
	        <li class="{{ Request::path() == 'tree-surgery' ? 'active' : '' }}"><a href="/tree-surgery" class="dark-green-text poppins">Tree Surgery</a></li>
	        <li class="{{ Request::path() == 'corporate-landscaping' ? 'active' : '' }}"><a href="/corporate-landscaping" class="dark-green-text poppins">Corporate Landscaping</a></li>
	        <li class="{{ Request::path() == 'garden-design' ? 'active' : '' }}"><a href="/garden-design" class="dark-green-text poppins">Garden Design</a></li>
	        <li class="{{ Request::path() == 'pimp-your-garden' ? 'active' : '' }}"><a href="/pimp-your-garden" class="dark-green-text poppins">Pimp Your Garden!</a></li>
	        <li class="{{ Request::path() == 'gallery' ? 'active' : '' }}"><a href="{{ route('gallery') }}" class="dark-green-text poppins">Gallery</a></li>
	        <li class="{{ Request::path() == 'contact' ? 'active' : '' }}"><a href="{{ route('contact') }}" class="dark-green-text poppins">Contact Us</a></li>
		</ul>

		<a href="#" data-activates="nav-mobile" class="button-collapse right"><i class="material-icons dark-green-text light">menu</i></a>
	
	</div>
</nav>




