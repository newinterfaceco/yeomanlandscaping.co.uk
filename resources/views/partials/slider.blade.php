<div class="slider">
	<ul class="slides">

		<li>
			<div class="overlay">
				<div class="caption left-align">
					<h3>Yeoman Landscaping Services</h3>
					<h5 class="light grey-text text-lighten-3">Yeoman Landscaping provides a full corporate landscaping service from one off maintenance visits through designing and implementing planting schemes and on to regular contract garden maintenance work.</h5>
					<br>
					<a href="/contact" class="btn-large waves-effect dark-green-btn white-text green-slider-button">Find Out More</a>
                    <a href="/contact" class="btn-large waves-effect white white-slider-button">Contact Us</a>
				</div>
			</div>
			<img src="/slides/1.png">
		</li>

		<li>
			<div class="overlay">
				<div class="caption left-align">
					<h3>Maintain the garden of your dreams</h3>
					<h5 class="light grey-text text-lighten-3">Yeoman Landscapes Service provides all of our customers with a level of workmanship and a commitment to customer service that is second to none. If you need regular maintenance service to keep your garden or grounds looking good all year round or just a garden close down or spring clean then choosing Yeoman Landscapers is probably the best step you can take.</h5>
					<br>
					<a href="/maintenance" class="btn-large waves-effect dark-green-btn white-text green-slider-button">Find Out More</a>
                    <a href="/contact" class="btn-large waves-effect white white-slider-button">Contact Us</a>
				</div>
			</div>
			<img src="/slides/2.JPG">
		</li>

		<li>
			<div class="overlay">
				<div class="caption left-align">
					<h3>Ground Breaking Ideas</h3>
					<h5 class="light grey-text text-lighten-3">Yeoman Landscapes offer a full range of hard landscaping services throughout Norfolk and Suffolk. We only use the best materials, emphasise the planning proccess and use proven construction techniques to ensure a quality finish and a long lasting solution to your landscaping needs.<br> Choose Yeoman Landscapes for your new Patio, Path or Driveway.</h5>
					<br>
					<a href="/hard-landscaping" class="btn-large waves-effect dark-green-btn white-text green-slider-button">Find Out More</a>
                    <a href="/contact" class="btn-large waves-effect white white-slider-button">Contact Us</a>
				</div>
			</div>
			<img src="/slides/3.JPG">
		</li>

		<li>
			<div class="overlay">
				<div class="caption left-align">
					<h3>We will go out on a limb for you!</h3>
					<h5 class="light grey-text text-lighten-3">Yeoman Landscapes provide domestic and commercial tree surgery services including pruning, planting, felling and hedge trimming to Norfolk and South Suffolk.<br>
                    Even if all you need is advice, we're here to help!</h5>
					<br>
					<a href="/tree-surgery" class="btn-large waves-effect dark-green-btn white-text green-slider-button">Find Out More</a>
                    <a href="/contact" class="btn-large waves-effect white white-slider-button">Contact Us</a>
				</div>
			</div>
			<img src="/slides/4.jpg">
		</li>

		<li>
			<div class="overlay">
				<div class="caption left-align">
					<h3>Landscaping for your Business or Organisation</h3>
					<h5 class="light grey-text text-lighten-3">Yeoman Landscapes provide corporate landscaping services to companies trading in Norfolk and South Suffolk. If you require a quote for regular maintenance or a new project, please contact us and we will get back to you within 24 hours.</h5>
					<br>
					<a href="/corporate-landscaping" class="btn-large waves-effect dark-green-btn white-text green-slider-button">Find Out More</a>
                    <a href="/contact" class="btn-large waves-effect white white-slider-button">Contact Us</a>
				</div>
			</div>
			<img src="/slides/5.JPG">
		</li>

		<li>
			<div class="overlay">
				<div class="caption left-align">
					<h3>Planning Your Gardening Dreams</h3>
					<h5 class="light grey-text text-lighten-3">The key to a good garden is good garden design. What will the garden be used for? Who will use the garden? How the garden will be maintained? Without answering these key questions and adapting your garden to match the answers you will never be happy with the results. Choose Yeoman Landscapes for a plan that creates your perfect garden.</h5>
					<br>
					<a href="/garden-design" class="btn-large waves-effect dark-green-btn white-text green-slider-button">Find Out More</a>
                    <a href="/contact" class="btn-large waves-effect white white-slider-button">Contact Us</a>
				</div>
			</div>
			<img src="/slides/6.JPG">
		</li>

		<li>
			<div class="overlay">
				<div class="caption left-align">
					<h3>Make Your Garden A Room To Live In</h3>
					<h5 class="light grey-text text-lighten-3">Sometimes a pretty garden can be turned into a beautiful garden or welcoming garden into a garden that is a complete extension of your home. Yeoman Landscapes will add the garden feature that finishes off your garden design - Ponds, Fountains, Lodges, Gazebos, Kitchen Gardens, Patios, Paths and decking. We do it all!</h5>
					<br>
					<a href="/garden-design" class="btn-large waves-effect dark-green-btn white-text green-slider-button">Find Out More</a>
                    <a href="/contact" class="btn-large waves-effect white white-slider-button">Contact Us</a>
				</div>
			</div>
			<img src="/slides/7.png">
		</li>

	</ul>
</div>