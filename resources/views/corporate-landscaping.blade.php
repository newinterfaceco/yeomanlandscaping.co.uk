@extends('layouts.app')

@section('title', 'Yeoman Corporate Landscaping')

@section('description', 'Yeoman Garden Design - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')

	<div class="parallax-container valign-wrapper breadcrumb-container">
		<div class="breadcrumb-overlay">
			<div class="container">
				<div class="row">
					<h5 class="header col s12 light white-text">Corporate Landscaping</h5>
				</div>
			</div>
		</div>
		<div class="parallax breadcrumb"><img src="/maintenance-images/6.JPG" alt="Maintenance image for Yeoman Landscapes, provider of Garden Maintenance, Hard Landscaping, Tree Surgery, Corporate Landscaping &amp; Garden Design to companies and individuals in South Norfolk, Central Norfolk and South Suffolk."></div>
	</div>

	<section class="page">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h1>Landscaping for your business or organisation</h1>

					<p>Yeoman Landscapes provide corporate landscapingservices to companies trading in South and Central Norfolk and South Suffolk.</p>

					<p>Yeoman Landscaping provides a full corporate landscaping service from one off maintenance visits through designing and implementing planting schemes and on to regular contract garden maintenance work.</p>

					<p>If you require a quote for regular maintenance or a new project just contact us using the contact form and we will get back to you within 24 hours.</p>

					<div class="divider"></div>

					<h2>We offer the following business landscaping services:</h2>

					<ul class="content-list">
						<li>Housing development landscaping services</li>
						<li>Lawn laying</li>
						<li>planting, weeding mulching hedge trimming</li>
						<li>hard landscaping</li>
						<li>contract maintenance</li>
						<li>Tree Surgery</li>
						<li>Chemical spraying</li>
						<li>Fencing repairs</li>
					</ul>

					<div class="divider"></div>

					<h2>Our Corporate Landscaping Portfolio:</h2>

					@include('partials.images')

				</div>
			</div>
		</div>	
	</section>

  	@include('partials.contact')

@endsection