@extends('layouts.app')

@section('title', 'Contact Yeoman Landscaping')

@section('description', 'Contact Yeoman Landscaping - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')

	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d77614.98846899086!2d1.0429242491119715!3d52.56114033678519!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d9c331829aa755%3A0x990e934c9342e77b!2sWymondham!5e0!3m2!1sen!2suk!4v1478808187373" width="100%" height="450" frameborder="0"></iframe>

	<section class="page">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h1>Contact Yeoman Landscaping</h1>

					<p>Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.</p>

					<p>As you would expect your garden needs a lot of maintenance during the growing seasons of spring and summer. It is however equally important in the autumn and winter months to keeping a well maintained garden, especially if you want to enjoy looking at it from the warmth of your armchair! We can offer you a regular maintenance contract or, alternatively, are quite happy to do a one off maintenance visit.</p>

					<p>If you require a quote for regular maintenance or a new project just contact us using the contact form and we will get back to you within 24 hours.</p>

					<h2>We offer the following Garden Maintenance and Landscaping services:</h2>

					<ul class="content-list">
						<li>Garden tidy/clearance</li>
						<li>Grass cutting</li>
						<li>Lawn care and maintenance</li>
						<li>Hedge cutting</li>
						<li>Weeding</li>
						<li>Patio/driveway pressure washing &amp; sealing</li>
						<li>Shrub/tree pruning</li>
						<li>Tree work/stump grinding</li>
						<li>Chemical spraying</li>
						<li>Mulching</li>
						<li>Planting</li>
						<li>Leaf clearing</li>
						<li>Fencing repairs</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

  	@include('partials.contact')

@endsection