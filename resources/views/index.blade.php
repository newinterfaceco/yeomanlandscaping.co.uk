@extends('layouts.app')

@section('title', 'Yeoman Landscaping')

@section('description', 'Yeoman Landscaping - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')
	@include('partials.slider')

	<section class="page">
		<div class="container">
			<div class="row">

				<div class="col s12 center">
					<h1>Garden Design, Landscaping &amp; Maintenance Services in Norfolk and Suffolk</h1>
				</div>

                <div class="col s12 m6 l4">
                    <div class="card">
                        <div class="card-image">
                            <img src="/services/1.png">
                            <span class="services-card-title card-title">Designing</span>
                        </div>
                        <div class="card-content services-card-content">
                            <p>Do you want to use your garden for outside entertaining? A place for your children to play? Simply relaxing? Or maybe a combination of uses? Whatever your plans for your garden,
                            You can rest assured your ideas and aspirations are the driving force behind everything we do.</p>
                        </div>
                        <div class="card-action">
                            <a href="/garden-design" class="btn waves-effect green darken-4 white-text">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l4">
                    <div class="card">
                        <div class="card-image">
                            <img src="/services/2.png">
                            <span class="services-card-title card-title">Corporate Landscaping</span>
                        </div>
                        <div class="card-content services-card-content">
                            <p>If you have a business or organisation in Norfolk or Suffolk which has regular or infrequent Landscaping or maintenance needs- Let Yeoman Landscaping know, and we will give you a highly competitive quote.</p>
                        </div>
                        <div class="card-action">
                            <a href="/corporate-landscaping" class="btn waves-effect green darken-4 white-text">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l4">
                    <div class="card">
                        <div class="card-image">
                            <img src="/services/3.png">
                            <span class="services-card-title card-title">Hard Landscaping</span>
                        </div>
                        <div class="card-content services-card-content">
                            <p>We offer a complete range of hard landscaping and garden building services. these include:- timber decking, patios paths and fencing -garden sheds, summer houses and pagodas. Whatever your hard landscaping needs contact Yeoman Landscaping for a compertitive quote.</p>
                        </div>
                        <div class="card-action">
                            <a href="/hard-landscaping" class="btn waves-effect green darken-4 white-text">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l4">
                    <div class="card">
                        <div class="card-image">
                            <img src="/services/4.png">
                            <span class="services-card-title card-title">Tree Surgery</span>
                        </div>
                        <div class="card-content services-card-content">
                            <p>We offer a full tree surgery service from simple dangerous branch removal, through tree pollarding or reshaping and on to full tree felling.We offer free advice and written quotations throughout South Norfolk, Norwich and North Suffolk, aim to advise you impartially, address your problems and not make any unnecessary recommendations.</p>
                        </div>
                        <div class="card-action">
                            <a href="/tree-surgery" class="btn waves-effect green darken-4 white-text">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l4">
                    <div class="card">
                        <div class="card-image">
                            <img src="/services/5.png">
                            <span class="services-card-title card-title">Garden Maintenance</span>
                        </div>
                        <div class="card-content services-card-content">
                            <p>We provide all of our garden maintenance customers with a level of workmanship and a commitment to customer service that is second to none..If you need a regular maintenance service to keep your garden or grounds looking good all year round or just a garden close down or spring clean then choosing Yeoman Landscaping is probably the best step you can take.</p>
                        </div>
                        <div class="card-action">
                            <a href="/maintenance" class="btn waves-effect green darken-4 white-text">Read More</a>
                        </div>
                    </div>
                </div>

                <div class="col s12 m6 l4">
                    <div class="card">
                        <div class="card-image">
                            <img src="/services/6.png">
                            <span class="services-card-title card-title">The Finishing Touch</span>
                        </div>
                        <div class="card-content services-card-content">
                            <p>Sometimes a pretty garden can be turned into beautiful garden or welcoming garden into a garden that is a complete extension to your home . Yeoman Landscaping will add the garden feature that finishes off your garden design -ponds, fountains, lodges, gazebos, kitchen gardens,patios,paths and decking we do it all!</p>
                        </div>
                        <div class="card-action">
                            <a href="/garden-design" class="btn waves-effect green darken-4 white-text">Read More</a>
                        </div>
                    </div>
                </div>

			</div>
		</div>
	</section>

  	@include('partials.contact')

@endsection