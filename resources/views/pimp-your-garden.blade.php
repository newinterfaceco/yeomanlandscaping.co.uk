@extends('layouts.app')

@section('title', 'Yeoman Pimp Your Garden')

@section('description', 'Yeoman Pimp Your Garden - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')

	<div class="parallax-container valign-wrapper breadcrumb-container">
		<div class="breadcrumb-overlay">
			<div class="container">
				<div class="row">
					<h5 class="header col s12 light white-text">Pimp Your Garden</h5>
				</div>
			</div>
		</div>
		<div class="parallax breadcrumb"><img src="/maintenance-images/6.JPG" alt="Maintenance image for Yeoman Landscapes, provider of Garden Maintenance, Hard Landscaping, Tree Surgery, Corporate Landscaping &amp; Garden Design to companies and individuals in South Norfolk, Central Norfolk and South Suffolk."></div>
	</div>

	<section class="page">
		<div class="container">
			<div class="row">
				<div class="col s12">
				
					<h1>Make your garden a room to live in</h1>

					<p>Sometimes a pretty garden can be turned into a beautiful garden or welcoming garden into a garden that is a comp,ete extension of your home.</p>

					<p>Yeoman Landscapes will add the garden feature that finishes off your garden design - Ponds, Fountains, Lodges, Gazebos, kitchen Gardens, Patios, Paths and Deckings.</p>

					<p>We do it all!</p>
					
					<p>If you require a quote for regular maintenance or a new project just contact us using the contact form and we will get back to you within 24 hours.</p>

					<div class="divider"></div>

					<h2>We offer the following Garden Features:</h2>

					<ul class="content-list">
						<li>Fountains</li>
						<li>Ponds</li>
						<li>Garden Walls and Fences</li>
						<li>Hedges</li>
						<li>Patios</li>
						<li>Driveways and Paths</li>
						<li>Lodges and Summer houses</li>
						<li>Mature Trees</li>
						<li>Decking</li>
						<li>Garden Gnomes!</li>
					</ul>

					<div class="divider"></div>

					<h2>Our Garden Portfolio:</h2>

					@include('partials.images')

				</div>
			</div>
		</div>
	</section>

  	@include('partials.contact')

@endsection