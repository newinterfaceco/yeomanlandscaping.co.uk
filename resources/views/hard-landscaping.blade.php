@extends('layouts.app')

@section('title', 'Yeoman Hard Landscaping')

@section('description', 'Yeoman Hard Landscaping - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')

	<div class="parallax-container valign-wrapper breadcrumb-container">
		<div class="breadcrumb-overlay">
			<div class="container">
				<div class="row">
					<h5 class="header col s12 light white-text">Hard Landscaping</h5>
				</div>
			</div>
		</div>
		<div class="parallax breadcrumb"><img src="/maintenance-images/6.JPG" alt="Maintenance image for Yeoman Landscapes, provider of Garden Maintenance, Hard Landscaping, Tree Surgery, Corporate Landscaping &amp; Garden Design to companies and individuals in South Norfolk, Central Norfolk and South Suffolk."></div>
	</div>


	<section class="page">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h1>Ground Breaking Ideas</h1>

					<p>Hard Landscaping is the structure of a garden.  It is the structure which defines much of your garden it provides the back drop and framing to your softer features such as trees, plants, water features and lawns. Starting off at the boundaries with walls or fences it continues through patios, pathways and walls and includes features such as driveways and decking.</p>

					<p>Hard Landscaping is crucial to the success of your garden. Getting this aspect of your garden right is essential, as hard landscaping wil provide the structure to your garden for a long time and thus must be constructed from good, reliable materials, which allow flexibility to the rest of your garden design as well as being pleasing to the eye.</p>

					<p>If you require a quote for regular maintenance or a new project just contact us using the contact form and we will get back to you within 24 hours.</p>

					<div class="divider"></div>

					<h2>We offer the following Hard Landscaping Services:</h2>

					<ul class="content-list">
						<li>Building retaining walls</li>
						<li>Posts and fences</li>
						<li>Patios and decking</li>
						<li>Creating or repairing paths</li>
						<li>Driveways and parking areas</li>
						<li>Rockeries</li>
						<li>Sunken or raised ponds – including cascades and fountains</li>
						<li>Garden security solutions</li>
						<li>Levelling ground</li>
					</ul>

					<div class="divider"></div>

					<h2>Our Hard Landscaping Portfolio:</h2>

					@include('partials.images')

				</div>
			</div>
		</div>
	</section>

  	@include('partials.contact')

@endsection