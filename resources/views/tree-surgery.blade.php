@extends('layouts.app')

@section('title', 'Yeoman Tree Surgery')

@section('description', 'Yeoman Tree Surgery - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')

	<div class="parallax-container valign-wrapper breadcrumb-container">
		<div class="breadcrumb-overlay">
			<div class="container">
				<div class="row">
					<h5 class="header col s12 light white-text">Tree Surgery</h5>
				</div>
			</div>
		</div>
		<div class="parallax breadcrumb"><img src="/maintenance-images/6.JPG" alt="Maintenance image for Yeoman Landscapes, provider of Garden Maintenance, Hard Landscaping, Tree Surgery, Corporate Landscaping &amp; Garden Design to companies and individuals in South Norfolk, Central Norfolk and South Suffolk."></div>
	</div>

	<section class="page">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h1>We will go out on a limb for you!</h1>

					<p>Yeoman Landscaping Tree Surgery Service provides a full assessment and treatment service for all your tree surgery needs. If you are concerned about a tree and need to check out its health or safety or to check for infestations and tree diseases such as powdery mildew or sooty mould.</p>

					<p>We can diagnose problems such as heart rot or canker and control methods can be employed to reduce chances of spread between nearby trees.</p>

					<p>Where major tree problems such as frost cracking has occurred it is unlikely that we can apply control methods, but we can check your trees for signs of limb weakness. This can help prevent branch failure and reduce the likelihood of fallen limbs affecting neighbouring properties or being a health and safety risk.</p>

					<p>Similarly, weather damaged trees affected by high winds can have dangerous or hung up limbs and we can remove these safely using appropriate tree surgery equipment to access the tree canopy.</p>

					<p>We can also assist with canopy reduction services, thinning foliage density to increase light to gardens and properties. Yeoman Landscapes Tree Surgery Services can also reduce the height of fast growing tree species such as conifers and leylandii hedging. In our area trees are often covered by Tree Preservation Orders (TPOs), which limit working on trees and prevent felling of trees. In these instances, it is important that you let us know and that we work with you and the relevant authority to legally maintain the tree within the law as near as possible to your requirements. Where a tree need to be felled, we can provide a number of tree services - these include felling and removal of crossing limbs, trunks and entire tree stumps. Stumps can be removed using chemical stump removal to prevent fungal growth in tree stump remains, or through stump grinding to reduce the wood to ground level.</p>

					<p>Wood from your cut down trees can either be cross cut to provide fire wood, left as logs for garden features, disposed of, or processed in wood chippers to provide garden mulch.</p>

					<p>If you require a quote for regular maintenance or a new project just contact us using the contact form and we will get back to you within 24 hours.</p>

					<div class="divider"></div>

					<h2>Examples of some of the tree Surgery Services we can offer are:</h2>

					<ul class="content-list">
						<li>Tree Assessment</li>
						<li>Pruning - Pollarding</li>
						<li>Felling</li>
						<li>Mulching</li>
						<li>Stump Grinding/Chemical Removal</li>
						<li>Emergency Callout</li>
						<li>Shrub/tree pruning</li>
						<li>Canopy Thinning</li>
						<li>Disease Control</li>
					</ul>

					<div class="divider"></div>

					<h2>Our Tree Surgery Portfolio:</h2>

					@include('partials.images')

				</div>
			</div>
		</div>
	</section>

  	@include('partials.contact')

@endsection