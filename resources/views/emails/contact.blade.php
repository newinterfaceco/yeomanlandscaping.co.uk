You received a message from yeomanlandscaping.co.uk:

<p>
Name: {{ $name }}
</p>

<p>
Email: {{ $email }}
</p>

<p>
Subject: {{ $subject }}
</p>

<p>
Message:<br><br> {{ $user_message }}
</p>