
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>@yield('title') - Landscaping, Maintenance, Hard Landscaping, Tree Surgery, Corporate Landscaping and Garden Design in Norfolk and Suffolk</title>
    <meta name="description" content="@yield('description')">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/app.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="shortcut icon" href="/favicon.ico">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
   <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136939836-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-136939836-2');
</script>

</head>
<body>

    <main>

        @include('partials.nav')

        @yield('content')

    </main>

    @include('partials.footer')

    <script src="/app.js"></script>

    @if(Session::has('contact_message'))
        <script type="text/javascript">
            Materialize.toast('Your contact form request has been sent!', 10000);
        </script>
    @endif

    @if(Session::has('testimonial_added'))
        <script type="text/javascript">
            Materialize.toast('Testimonial added!', 10000);
        </script>
    @endif

    @if(Session::has('testimonial_deleted'))
        <script type="text/javascript">
            Materialize.toast('Testimonial deleted!', 10000);
        </script>
    @endif

    @if(Session::has('image_deleted'))
        <script type="text/javascript">
            Materialize.toast('Image Deleted!', 10000);
        </script>
    @endif

    @if(Session::has('logout'))
        <script type="text/javascript">
            Materialize.toast('You have been logged out', 10000);
        </script>
    @endif

    @if(Session::has('password_success'))
        <script type="text/javascript">
            Materialize.toast('Password Updated', 10000);
        </script>
    @endif

    @if(Session::has('password_fail'))
        <script type="text/javascript">
            Materialize.toast('Passwords did not match', 10000);
        </script>
    @endif

    @if(Session::has('no_password'))
        <script type="text/javascript">
            Materialize.toast('There was no password', 10000);
        </script>
    @endif
    
    </body>
</html>