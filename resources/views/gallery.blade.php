@extends('layouts.app')

@section('title', 'Yeoman Landscaping Gallery')

@section('description', 'Yeoman Landscaping Gallery - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')

	<div class="parallax-container valign-wrapper breadcrumb-container">
		<div class="breadcrumb-overlay">
			<div class="container">
				<div class="row">
					<h5 class="header col s12 light white-text">Gallery</h5>
				</div>
			</div>
		</div>
		<div class="parallax breadcrumb"><img src="/maintenance-images/6.JPG" alt="Maintenance image for Yeoman Landscapes, provider of Garden Maintenance, Hard Landscaping, Tree Surgery, Corporate Landscaping &amp; Garden Design to companies and individuals in South Norfolk, Central Norfolk and South Suffolk."></div>
	</div>

	<section class="page">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h1>Gallery</h1>

					<p>Yeoman Landscapes will add the garden feature that finishes off your garden design - Ponds, Fountains, Lodges, Gazebos, kitchen Gardens, Patios, Paths and Deckings.</p>

					<p>If you require a quote for regular maintenance or a new project just contact us using the contact form and we will get back to you within 24 hours.</p>

					<div class="divider"></div><br>

					<div class="row">
						@if($images->count() == 0)
							<div class="col s12 m6 l4">
								<p>There are no images in the gallery.</p>
								<br>
							</div>
						@else
							@foreach($images as $image)
								<div class="col s12 m6 l4">
									<div class="material-placeholder"><img class="materialboxed gallery-image" src="/img/750/750/{{ $image->url }}"></div>
								</div>
							@endforeach
						@endif
					</div>

				</div>
			</div>
		</div>
	</section>

  	@include('partials.contact')

@endsection