@extends('layouts.app')

@section('title', 'Yeoman Garden Maintenance')

@section('description', 'Yeoman Garden Maintenance - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')

	<div class="parallax-container valign-wrapper breadcrumb-container">
		<div class="breadcrumb-overlay">
			<div class="container">
				<div class="row">
					<h5 class="header col s12 light white-text">Maintenance</h5>
				</div>
			</div>
		</div>
		<div class="parallax breadcrumb"><img src="/maintenance-images/6.JPG" alt="Maintenance image for Yeoman Landscapes, provider of Garden Maintenance, Hard Landscaping, Tree Surgery, Corporate Landscaping &amp; Garden Design to companies and individuals in South Norfolk, Central Norfolk and South Suffolk."></div>
	</div>

	<section class="page">

		<div class="container">
			<div class="row">
				<div class="col s12">
					<h1>Yeoman Garden Maintenance - Wymondham, Norwich, Norfolk</h1>

					<p>A beautiful garden requires constant maintenance and as you would expect your garden needs more maintenance during the growing seasons of spring and summer.</p>

					<p>Do not neglect it during the close down months of autumn and winter.  The work you do during these months lays the groundworks for the early opening of your garden in the spring.</p>

					<p>We can offer you a regular maintenance contract or, alternatively, we are quite happy to do a one off maintenance visit</p>

					<p>If you require a quote for regular maintenance or a new project just contact us using the contact form and we will get back to you within 24 hours.</p>

					<div class="divider"></div>

					<h2>We offer the following Garden Maintenance Services:</h2>

					<ul class="content-list">
						<li>Garden tidy/clearance</li>
						<li>Grass cutting</li>
						<li>Lawn feeding and scaryfying</li>
						<li>Hedge cutting</li>
						<li>Weeding</li>
						<li>Composting</li>
						<li>Patio/driveway pressure washing &amp; sealing</li>
						<li>Shrub/tree pruning and pollarding</li>
						<li>Fruit collection
						</li><li>Tree work/stump grinding</li>
						<li>Chemical spraying</li>
						<li>Mulching</li>
						<li>Planting</li>
						<li>Leaf clearing</li>
						<li>Fencing and wall repairs</li>
					</ul>

					<div class="divider"></div>

					<h2>Our Garden Maintenance Portfolio:</h2>

					@include('partials.images')

				</div>
			</div>
		</div>

	</section>

  	@include('partials.contact')

@endsection