@extends('layouts.app')

@section('content')
<section id="admin">
    <div class="container">
        <div class="row">
            <div class="col s12 m3 l2">
                <ul class="section table-of-contents">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/gallery" class="active">Gallery</a></li>
                    <li><a href="/admin/testimonial">Testimonials</a></li>
                    <li><a href="/admin/password">Password</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </div>
            <div class="col s12 m9 l10">
                <div class="card">
                    <div class="card-title">Gallery</div>
                    <br>
                    <ul class="collapsible" data-collapsible="accordion">
                        <li>
                          <div class="collapsible-header"><i class="material-icons grey-text">photo_library</i>Maintenance</div>
                          <div class="collapsible-body">
                              <form action="/admin/gallery/1" class="dropzone" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                              </form>
                              <div id="maintenance-gallery"><p class="empty">There are no images for this service.</p></div>
                          </div>
                        </li>
                        <li>
                          <div class="collapsible-header"><i class="material-icons grey-text">photo_library</i>Hard Landscaping</div>
                          <div class="collapsible-body">
                              <form action="/admin/gallery/2" class="dropzone" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                              </form>
                              <div id="landscaping-gallery"><p class="empty">There are no images for this service.</p></div>
                          </div>
                        </li>
                        <li>
                          <div class="collapsible-header"><i class="material-icons grey-text">photo_library</i>Tree Surgery</div>
                          <div class="collapsible-body">
                              <form action="/admin/gallery/3" class="dropzone" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                              </form>
                              <div id="tree-gallery"><p class="empty">There are no images for this service.</p></div>
                          </div>
                        </li>
                        <li>
                          <div class="collapsible-header"><i class="material-icons grey-text">photo_library</i>Corporate Landscaping</div>
                          <div class="collapsible-body">
                              <form action="/admin/gallery/4" class="dropzone" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                              </form>
                              <div id="corporate-gallery"><p class="empty">There are no images for this service.</p></div>
                          </div>
                        </li>
                        <li>
                          <div class="collapsible-header"><i class="material-icons grey-text">photo_library</i>Garden Design</div>
                          <div class="collapsible-body">
                              <form action="/admin/gallery/5" class="dropzone" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                              </form>
                              <div id="design-gallery"><p class="empty">There are no images for this service.</p></div>
                          </div>
                        </li>
                        <li>
                          <div class="collapsible-header"><i class="material-icons grey-text">photo_library</i>Pimp Your Garden</div>
                          <div class="collapsible-body">
                              <form action="/admin/gallery/6" class="dropzone" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                              </form>
                              <div id="pimp-gallery"><p class="empty">There are no images for this service.</p></div>
                          </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
