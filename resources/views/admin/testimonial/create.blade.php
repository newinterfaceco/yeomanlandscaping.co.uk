@extends('layouts.app')

@section('content')
<section id="admin">
    <div class="container">
        <div class="row">
            <div class="col s12 m3 l2">
                <ul class="section table-of-contents">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/gallery">Gallery</a></li>
                    <li><a href="/admin/testimonial" class="active">Testimonials</a></li>
                    <li><a href="/admin/password">Password</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </div>
            <div class="col s12 m9 l10">
                <div class="card">
                    <div class="card-title">Create Testimonial</div>
                    <div class="row">
                    <br>
                        <form class="col s12" role="form" method="POST" action="{{ url('/admin/testimonial') }}">
                        {{ csrf_field() }}
							             <div class="row">
                            <div class="input-field col s6">
                              <input name="name" id="name" type="text" class="validate">
                              <label for="name">Name</label>
                            </div>
                            <div class="input-field col s12">
                              <input name="content" id="content" type="text" class="validate">
                              <label for="content">Testimonial</label>
                            </div>
                          </div>    
                          <div class="row">
                              <div class="col s12">
                              <button type="submit" class="btn dark-green-btn">Add Testimonal</button>
                              </div>
                          </div>
                      </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
