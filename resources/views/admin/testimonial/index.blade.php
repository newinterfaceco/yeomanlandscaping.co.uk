@extends('layouts.app')

@section('content')
<section id="admin">
    <div class="container">
        <div class="row">
            <div class="col s12 m3 l2">
                <ul class="section table-of-contents">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/gallery">Gallery</a></li>
                    <li><a href="/admin/testimonial" class="active">Testimonials</a></li>
                    <li><a href="/admin/password">Password</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </div>
            <div class="col s12 m9 l10">
                <div class="card">
                    <div class="row">
                        <div class="col s8">
                            <div class="card-title">Testimonials</div>  
                        </div>
                        <div class="col s4">
                            <a href="/admin/testimonial/create" class="btn dark-green-btn right">add</a>  
                        </div>
                    </div>
                    
                    @if($testimonials->count() == 0)
                    <p>There are no testimonials.</p>
                    @else
                      <ul class="collapsible" data-collapsible="accordion">
                      @foreach($testimonials as $testimonial)
                          <li>
                            <div class="collapsible-header"><i class="material-icons grey-text">format_quote</i>{{ $testimonial->name }}
                              <a href="/admin/testimonial/{{ $testimonial->id }}/edit" class="right"><i class="material-icons red-text">close</i></a>
                            </div>
                            <div class="collapsible-body"><p>{{ $testimonial->content }}</p></div>
                          </li>
                      @endforeach
                       </ul>
                    @endif
                   
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
