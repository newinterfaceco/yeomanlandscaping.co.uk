@extends('layouts.app')

@section('content')
<section id="admin">
    <div class="container">
        <div class="row">
            <div class="col s12 m3 l2">
                <ul class="section table-of-contents">
                    <li><a href="/admin/dashboard" class="active">Dashboard</a></li>
                    <li><a href="/admin/gallery">Gallery</a></li>
                    <li><a href="/admin/testimonial">Testimonials</a></li>
                    <li><a href="/admin/password">Password</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </div>
            <div class="col s12 m9 l10">
                <div class="card">
                    <div class="card-title">Hello, {{ auth()->user()->name }}</div>
                    <p>You are logged in!</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
