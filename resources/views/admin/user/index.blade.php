@extends('layouts.app')

@section('content')
<section id="admin">
    <div class="container">
        <div class="row">
            <div class="col s12 m3 l2">
                <ul class="section table-of-contents">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/gallery">Gallery</a></li>
                    <li><a href="/admin/testimonial">Testimonials</a></li>
                    <li><a href="/admin/password" class="active">Password</a></li>
                    <li><a href="/logout">Logout</a></li>
                </ul>
            </div>
            <div class="col s12 m9 l10">
                <div class="card">
                    <div class="card-title">Password</div>
                    <div class="row">
                    <br>
                        <form class="col s12" role="form" method="POST" action="{{ url('/admin/password') }}">
                        {{ csrf_field() }}
                          <div class="row">
                            <div class="input-field col s6">
                              <input name="password" id="password" type="text" class="validate">
                              <label for="password">Password</label>
                            </div>
                            <div class="input-field col s6">
                              <input name="password_confirmation" id="password_confirmation" type="text" class="validate">
                              <label for="password_confirmation">Password Confirmation</label>
                            </div>
                          </div>    
                          <div class="row">
                              <div class="col s12">
                              <button type="submit" class="btn dark-green-btn">Update</button>
                              </div>
                          </div>
                      </form>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
