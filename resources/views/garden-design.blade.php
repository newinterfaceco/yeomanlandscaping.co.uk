@extends('layouts.app')

@section('title', 'Yeoman Garden Design')

@section('description', 'Yeoman Garden Design - Yeoman Landscaping provide landscaping services, Garden maintenance, tree surgery and hard landscaping companies and individuals in South and Central Norfolk and South Suffolk.')

@section('content')

	<div class="parallax-container valign-wrapper breadcrumb-container">
		<div class="breadcrumb-overlay">
			<div class="container">
				<div class="row">
					<h5 class="header col s12 light white-text">Garden Design</h5>
				</div>
			</div>
		</div>
		<div class="parallax breadcrumb"><img src="/maintenance-images/6.JPG" alt="Maintenance image for Yeoman Landscapes, provider of Garden Maintenance, Hard Landscaping, Tree Surgery, Corporate Landscaping &amp; Garden Design to companies and individuals in South Norfolk, Central Norfolk and South Suffolk."></div>
	</div>

	<section class="page">
		<div class="container">
			<div class="row">
				<div class="col s12">
					<h1>Planning your gardening dreams</h1>

					<p>Yeoman Landscaping Garden Design Service understands that a garden has to fulfil a whole variety of needs. Sometimes, a haven of peace, relaxation and contemplation, and sometimes a place for entertainment, outdoor dining, a peaceful breakfast in the sun and sometimes just partying!</p>

					<p>Whatever the event or day to day activity the garden should stimulate your senses, should reflect your mood and personality, and above all should grow with you and be maintainable within your budget and understanding of gardening.</p>

					<p>Yeoman Landscaping aims to design your garden with a balance of these aims in mind and will work with you to help formalise your ideas and aspirations and formulate them into a long lasting practical design. A design we want to be not only pleasing to the eye but also one that will provide you with years of practical enjoyment.</p>

					<p>Yeoman Landscaping not only design gardens but also construct and maintain them and we know that by choosing to work with us you will achieve the best result for what you want - not what we the garden design company think you need.</p>

					<p>If you require a quote for regular maintenance or a new project just contact us using the contact form and we will get back to you within 24 hours.</p>
					
					<div class="divider"></div>

					<h2>When we design a garden we:</h2>

					<ul class="content-list">
						<li>Listen to your needs</li>
						<li>Listen again</li>
						<li>Prepare a second stage design</li>
						<li>Listen again</li>
						<li>Review all aspects of the project</li>
						<li>Provide a final design complete with costings</li>
					</ul>

					<div class="divider"></div>

					<h2>Our Garden Design Portfolio:</h2>

					@include('partials.images')

				</div>
			</div>
		</div>
	</section>

  	@include('partials.contact')

@endsection