const elixir = require('laravel-elixir');

elixir(mix => {

    mix.less([
    	'materialize.less', 
    	'app.less'
	], 'public/app.css');

	mix.scripts([
		'jquery.js', 
		'materialize.js',
		'sweetalert.js',
		'app.js',
		'dropzone.js',
		'gallery.js'
	], 'public/app.js')

});
